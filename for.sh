#!/bin/bash
# liwl@nsccwx

# 本部分主要介绍for.sh
:<<EOF
	for 主要用来遍历可遍历的对象,比如
	1. 数组
	2. 特殊字符隔开的字符串
	3. 文本行
	4. 可生成特定对象的语句
	for 主要实现了以特殊字符
		(比如默认以空格或者换行符,特殊情况下通过IFS设置)
	隔开的列表为遍历对象的
EOF

# 1. 遍历数组
a=(100 200 300)
for i in ${a[*]}
do
	echo "$i"
done
# 2. 遍历可遍历对象,需要考虑IFS内置变量
:<<EOF
b="
	name
	age
	sex
	mony
"
EOF
b="name age sex money"
for i in ${b}
do
	echo ${i}
done
# 3 文本行
# 4. 可生成特定对象的语句
## 4.1
for i in $(seq 1 100)
do
	echo $i
done
## 4.2
for i in $(ls)
do
	echo $i
done
## 4.3
for i in 1 2 3 4
do
	echo $i
done
## 4.4 
for i in {1..10}
do
	echo $i
done
