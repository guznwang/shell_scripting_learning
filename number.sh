#!/bin/bash
# liwl@nsccwx

# 本部分介绍shell的数据类型：数字


# 1. 判断是否是数字:
## 1.1 直接判断
read -p "输入数字1:" num01
if !( echo ${num01}|egrep -q '[^0-9]')
then
	echo "你输入的是一个数"
fi

## 1.2 编写函数
function isNum(){
	local num=$1
	if [ $# -ne 1 ]
	then
		echo "call function:${FUNCNAME[0]} failed."
		exit 0
	fi
	if !(echo ${num}|egrep -q '[^0-9]')
	then
		#是数字返回1
		echo "1"
	else
		#不是数字返回0
		echo "0"
	fi
}

## 调用函数示例
read -p "输入数字2:" num02

rtn=$(isNum ${num02})
if [ ${rtn} -eq 1 ]
then
	echo "${num02} is a number"
elif [ ${rtn} -eq 0 ]
then
	echo "${num02} is not a number"
fi

# 2. 数字计算
## 2.1 关系运算
#for i in 1 2
#for i in {1..2}
for i in $(seq -w 1 2)
do
	read -p "输入第${i}个数:" num0${i} #变量也可以拼接
done

if [ ${num01} -gt ${num02} ]
then
	echo "${num01} > ${num02}"
fi

## 2.2 算数运算
echo "${num01}+${num02}=$[${num01}+${num02}]"
echo "${num01}-${num02}=$[${num01}-${num02}]"
echo "${num01}*${num02}=$[${num01}*${num02}]"
echo "${num01}/${num02}=$[${num01}/${num02}]"
echo "${num01}%${num02}=$[${num01}%${num02}]"
