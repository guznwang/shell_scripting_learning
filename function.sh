#!/bin/bash
# liwl@nsccwx

# 一. 传参
# 函数传参数，参数个数判断
function print_argv(){
	local argv=$1
	if [ $# -ne 1 ];then
		echo "call function:${FUNCNAME[0]} failed."
		exit 0
	fi
	echo "${argv}"
}

print_argv liwanliang
#print_argv $1

# 函数传入列表
a="1 2 3 4"

function print_list(){
	local list=$*
	for i in ${list}
	do
		echo ${i}
		echo "---"
	done
}
print_list ${a}
print_list A B C D

#当创建数组时,需要对数组元素进行解析,传给函数
b=(12 34 56)
print_list ${b[*]}

# 二. 返回值

function sum(){
	local f=$1
	local s=$2
	echo $[$f+$s]
}

mysum=$(sum 10 20)
echo ${mysum}

:<<EOF
	如果函数里面调用了系统命令/脚本等其他可执行文件,类似启动子进程的方式
	需要判断调用是否成功。
	此时子进程有返回，函数本身也有返回，相当于两个返回值
EOF

# 三. 函数调用
#脚本中调用函数时,只需要函数名即可
#比如:
#function myfunc_01(){ echo }
#myfunc_01
#但是此时函数的返回,返回到脚本外,即终端/重定向源等
##如果想要捕获函数的返回值,需要像调用系统命令一样,通过$(function)的方式
#func_rtn=$(myfunc_01),func_rtn就是函数的返回


# 3.1 函数调用别的函数
function func_01(){
	echo "Hi,I am function:${FUNCNAME[0]}"
}
function func_02(){
	func_01
	echo "Hi,I am function:${FUNCNAME[0]}"
}
func_02

## 3.2 函数调用自己
function func_03(){
	local num=$1
	echo "Now,I am ${num}"
	if [ $# -ne 1 ];then
		echo "call function:${FUNCNAME[0]} failed."
		echo "call as ${FUNCNAME[0]} [num](num<100)"
		exit 0
	fi
	num=$[${num}+1]
	if [ ${num} -le 100 ];then
		func_03 ${num}
	fi
}
func_03 1
