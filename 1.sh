#!/bin/bash

# 本脚本的函数是解析命令行上的数值，使其转为逗号隔开的数值

function h(){
	IFS=","
	for i in $*
	do
		has_flog=$(echo "$i"|grep "-"|wc -l)
		if [ "$a" != "0" ];then
			for j in $(seq ${i%-*} ${i#*-})
			do
				echo "$j"
			done
		else
			echo -ne "\b"
		fi
	done | tr '\n' ','|sed 's/.$//g'
	echo 
}

h $*
