#!/bin/bash
# liwl@nsccwx

# 本部分主要试验shell数据类型：字符(串)

# 1. 字符串
a="/home/export/online1/liwl"
echo ${a}

# 2. 字符串处理
## 2.1 长度
a="/home/export/online1/liwl"
b="/home/export/online1/liwl/test"
c="/home/export/online1/liwl/test "
echo "len(a):${#a},len(b):${#b},len(c):${#c}"

## 2.2 字符串比较
# 注意：字符变量解析时，一定要加双引号，否则脚本会有报错信息
# 下面的写法会有报错
#NAME="LiwanLiang"
#if [ ${NAME} == "Liwanliang" ];then
#	echo "name is liwanlaing"
#fi
#read -p "输入你的姓名:" NAME
#if [ "${NAME}" == "LiwanLiang" ];then
#	echo "你输入了正确的姓名:LiwanLiang"
#else
#	echo "输入了错误的姓名!"
#fi

## 3. 计算
## 3.1 算数计算
a="ABcd"
b="abCD"
#echo ${a}${b}
#echo ${a}+${b}
#echo ${a}-${b}
#echo ${a}*${b}
#echo ${a}/${b}
#echo ${a}%${b}

## 3.2 关系计算

# 4. 空字符串
a=
if [ -z ${a} ];then
	echo "ok"
	echo "${#a}"
fi
b=""
if [ -z ${b} ];then
	echo "ok"
	echo "${#b}"
fi
c=" "
if [ -z ${c} ];then
	echo "ok"
	echo "${#c}"
fi

if [ "${a}" == "${b}" ];then
	echo "a=b"
else
	echo "a!=b"
fi

if [ "${a}" == "${c}" ];then
	echo "a=c"
else
	echo "a!=c"
fi

if [ "${b}" == "${c}" ];then
	echo "b=c"
else
	echo "b!=c"
fi
d="\t"
if [ "${c}" == "${d}" ];then
	echo "c=d"
else
	echo "c!=d"
fi
