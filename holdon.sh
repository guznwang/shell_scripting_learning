#!/bin/bash
# 本脚本用于测试：询问等待，超时后设置为默认值
# 采用函数调用自己的方式，会存在多次输入的情况.bug
# 采用while循环的方式，能够正确使用

echo "从[1,2,3]选择"
function holdon(){
	read -p "请输入你的值:" youranswer
	if [ -d ${youranswer} ];then
		youranswer=1
	else
		while [ "${youranswer}" != "1" ] && [ "${youranswer}" != "2" ] && [ "${youranswer}" != "3" ]
		do
			echo "请重新输入:"
			read -p "请输入你的值:" youranswer
		done
	fi
	echo "你的输入是:${youranswer}"
}
holdon
