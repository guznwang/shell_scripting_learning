#!/bin/bash
# liwl@nsccwx

#本部分主要介绍数组的使用

# 1. 数组定义
int_array_01=(1 2 3 4 5)

# 数组可以写成以下方式,并且可以注释
str_array_01=(
	q_sw_expr
	q_sw_imcerr
	#q_sw_nofull
	q_sw_share
)

# 2. 数组长度
echo ${#int_array_01[*]}
echo ${#str_array_01[*]}

# 3. 数组元素
for i in ${str_array_01[*]}
do
	echo "${i}"
done

# 4. 数组的索引与元素
array_len=${#str_array_01[*]}
for i in $(seq 0 $[array_len-1])
do
	echo "str_array_01[$i] is ${str_array_01[$i]}"
done
