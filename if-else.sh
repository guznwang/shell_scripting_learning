#!/bin/bash
# liwl@nsccwx

# 1. if
# 条件是特例
for i in $(seq 1 100)
do
	# 对特例的处理
	if [ $[ ${i} % 2] -eq 0 ];then
		echo "$i是偶数"
	fi
done

# 1. if-else
# 结果是对立的
for i in $(seq 1 100)
do
	# 结果是对立的
	if [ $[${i}%2] -eq 0 ];then
		echo "${i}是偶数"
	else
		echo "${i} 是奇数"
	fi
done

# 3. if-elif-elif-else
# 多结果
read -p "输入一个数:" num
if [ ${num} -gt 0 ];then
	echo "${num} > 0"
elif [ ${num} -eq 0 ];then
	echo "${num} = 0"
elif [ ${num} -lt 0 ];then
	echo "${num} < 0"
fi

# 4. if-else:if-else
# 嵌套
read -p "input a num:" num01
if [ ${num01} -gt 0 ];then
	if [ ${num01} -gt 10 ];then
		echo "${num01} > 10"
	else
		echo "${num01} < 10"
	fi
	echo "${num01} > 0"
fi

# 5. 多条件
read -p "输入num01:" num01
read -p "输入num02:" num02
if [ ${num01} -gt 0 ] && [ ${num02} -gt 0 ];then
	echo "${num01}>0,${num02}>0"
elif [ ${num01} -lt 0 ] && [ ${num02} -gt 0 ];then
	echo "${num01}<0,${num02}>0"
elif [ ${num01} -lt 0 ] && [ ${num02} -lt 0 ];then
	echo "${num01}<0,${num02}<0"
else [ ${num01} -gt 0 ] && [ ${num02} -lt 0 ]
	echo "${num01}>0,${num02}<0"
fi


# 6. case选择
while true
do
	read -p "输入" NUM01
	case ${NUM01} in
	[0-9])
		echo "输入了数字"	
		;;
	[a-z])
		echo "输入了小写字母"
		;;
	[A-Z])
		echo "输入了大写字母"
		;;
	quit)
		exit 0
		;;
	*)
		echo "输入错误"
		;;
	esac
done
# 使用示例
function ask(){
	read -p "你确定此操作吗？(y/n):" ANSWER
	if [ "${ANSWER}" == 'y' ];then
		echo "你将执行操作"
	elif [ "${ANSWER}" == 'n' ];then
		echo "你将放弃操作!"
	else
		echo "输入错误!重新输入"
		ask
	fi
}
ask
