#!/bin/bash
# liwl@nsccwx

# 1. 无条件判断
# while [ 1 ] 等同于 while true
:<<EOF
while true
do
	read -p "是否退出(y/n):" answer
	if [ "${answer}" == "y" ];then
		echo "选择退出."
		exit 0
	else
		echo "未退出"
	fi
done
EOF
# 2. 有条件判断
:<<EOF
read -p "input a:" a
while [ ${a}  -gt 0 ]
do
	read -p "input a:" a
done

EOF

a=10
while [ ${a} -le 100 ]
do
	a=$[${a}+1]
	echo ${a}
done
# 代码示例
ANSWER=
while [ "${ANSWER}" != 'y' ] && [ "${ANSWER}" != 'n' ]
do
	read -p "输入你的选择(y/n):" ANSWER
	if [ "${ANSWER}" == 'y' ];then
		echo "你输入了y"
	elif [ "${ANSWER}" == 'n' ];then
		echo "你输入了n"
	else	
		echo "输入错误,重新输入"
		continue
	fi
done


# until
